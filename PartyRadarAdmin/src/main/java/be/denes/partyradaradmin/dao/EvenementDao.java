package be.denes.partyradaradmin.dao;

import java.sql.*;
import java.util.ArrayList;

import be.denes.partyradaradmin.model.Evenement;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
/**
 *
 * @author Maarten Heylen
 */
public class EvenementDao {

	public static ArrayList<Evenement> getEvenementen() {
		ArrayList<Evenement> resultaat = new ArrayList<Evenement>();
		try {
			ResultSet mijnResultset = Database.voerSqlUitEnHaalResultaatOp("SELECT * from Evenement");
			if (mijnResultset != null) {
				while (mijnResultset.next()) {
					Evenement huidigeEvenement = converteerHuidigeRijNaarObject(mijnResultset);
					resultaat.add(huidigeEvenement);
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}

		return resultaat;
	}

	public static Evenement getEvenementById(int id) {
		Evenement resultaat = null;
		try {
			ResultSet mijnResultset = Database.voerSqlUitEnHaalResultaatOp("SELECT * from Evenement where Evenement_id = ?", new Object[] { id });
			if (mijnResultset != null) {
				mijnResultset.first();
				resultaat = converteerHuidigeRijNaarObject(mijnResultset);
                                
      
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}

		return resultaat;
	}

  
    public static int voegEvenementToe(Evenement nieuweEvenement) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("INSERT INTO Evenement (Promotie_id, Titel, Beschrijving, Soort, Locatie, Prijs, Datum, Tijd, Categorie) VALUES (?,?,?,?,?,?,?,?,?)", new Object[] { nieuweEvenement.getPromotie_id(), nieuweEvenement.getTitel(), nieuweEvenement.getBeschrijving(), nieuweEvenement.getSoort(), nieuweEvenement.getLocatie(), nieuweEvenement.getPrijs(), nieuweEvenement.getDatum(), nieuweEvenement.getTijd(), nieuweEvenement.getCategorie() });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	public static int updateEvenement(Evenement nieuweEvenement) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("UPDATE Evenement SET Promotie_id = ?, Titel = ?, Beschrijving = ?, Soort = ?, Locatie = ?, Prijs = ?, Datum = ?, Tijd = ?, Categorie = ?  WHERE Evenement_id = ?", new Object[] {  nieuweEvenement.getTitel(), nieuweEvenement.getBeschrijving(), nieuweEvenement.getSoort(), nieuweEvenement.getLocatie(), nieuweEvenement.getPrijs(), nieuweEvenement.getDatum(), nieuweEvenement.getTijd(), nieuweEvenement.getCategorie() });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	public static int verwijderEvenement(int Evenement_id) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("DELETE FROM Evenement WHERE Evenement_id = ?", new Object[] { Evenement_id });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	private static Evenement converteerHuidigeRijNaarObject(ResultSet mijnResultset) throws SQLException {;
        ZonedDateTime beginTijdstip = ZonedDateTime.ofInstant(Instant.ofEpochMilli(mijnResultset.getTimestamp("Datum").getTime()), ZoneId.systemDefault());    
		return new Evenement(mijnResultset.getInt("Evenement_id"), mijnResultset.getInt("Promotie_id"), mijnResultset.getString("Titel"), mijnResultset.getString("Beschrijving"), mijnResultset.getString("Soort"), mijnResultset.getString("Locatie"), mijnResultset.getDouble("Prijs"), beginTijdstip , mijnResultset.getString("Tijd"), mijnResultset.getString("Categorie"));
	}

}