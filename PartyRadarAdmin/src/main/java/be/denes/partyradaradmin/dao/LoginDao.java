package be.denes.partyradaradmin.dao;

import java.sql.*;
import java.util.ArrayList;

import be.denes.partyradaradmin.model.Login;
/**
 *
 * @author Maarten Heylen
 */
public class LoginDao {

	public static ArrayList<Login> getLogins() {
		ArrayList<Login> resultaat = new ArrayList<Login>();
		try {
			ResultSet mijnResultset = Database.voerSqlUitEnHaalResultaatOp("SELECT * from Login");
			if (mijnResultset != null) {
				while (mijnResultset.next()) {
					Login huidigeLogin = converteerHuidigeRijNaarObject(mijnResultset);
					resultaat.add(huidigeLogin);
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}

		return resultaat;
	}

	public static Login getLoginById(int Login_id) {
		Login resultaat = null;
		try {
			ResultSet mijnResultset = Database.voerSqlUitEnHaalResultaatOp("SELECT * from Login where Login_id = ?", new Object[] { Login_id });
			if (mijnResultset != null) {
				mijnResultset.first();
				resultaat = converteerHuidigeRijNaarObject(mijnResultset);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}

		return resultaat;
	}

	public static int voegLoginToe(Login nieuweLogin) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("INSERT INTO Login (Login_id, Naam, Info, Profielfoto, Wachtwoord) VALUES (?,?)", new Object[] { nieuweLogin.getLogin_id(), nieuweLogin.getNaam(), nieuweLogin.getInfo(), nieuweLogin.getProfielfoto(), nieuweLogin.getWachtwoord()});
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	public static int updateLogin(Login nieuweLogin) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("UPDATE Login SET Naam = ?, Info = ?, Profielfoto = ?, Wachtwoord = ?  WHERE Login_id = ?", new Object[] { nieuweLogin.getNaam(), nieuweLogin.getInfo(), nieuweLogin.getProfielfoto(), nieuweLogin.getWachtwoord() });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	public static int verwijderLogin(int Login_id) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("DELETE FROM Login WHERE Login_id = ?", new Object[] { Login_id });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	private static Login converteerHuidigeRijNaarObject(ResultSet mijnResultset) throws SQLException {
		return new Login(mijnResultset.getInt("Login_id"), mijnResultset.getString("Naam"), mijnResultset.getString("Info"), mijnResultset.getBytes("Profielfoto"), mijnResultset.getString("Wachtwoord"));
	}

}
