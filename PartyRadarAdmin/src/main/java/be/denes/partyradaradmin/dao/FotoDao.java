package be.denes.partyradaradmin.dao;

import java.sql.*;
import java.util.ArrayList;


import be.denes.partyradaradmin.model.Foto;
/**
 *
 * @author Maarten Heylen
 */
public class FotoDao {

	public static ArrayList<Foto> getFotos() {
		ArrayList<Foto> resultaat = new ArrayList<Foto>();
		try {
			ResultSet mijnResultset = Database.voerSqlUitEnHaalResultaatOp("SELECT * from Foto");
			if (mijnResultset != null) {
				while (mijnResultset.next()) {
					Foto huidigeFoto = converteerHuidigeRijNaarObject(mijnResultset);
					resultaat.add(huidigeFoto);
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}

		return resultaat;
	}

	public static Foto getFotoById(int Foto_id) {
		Foto resultaat = null;
		try {
			ResultSet mijnResultset = Database.voerSqlUitEnHaalResultaatOp("SELECT * from Foto where Foto_id = ?", new Object[] { Foto_id });
			if (mijnResultset != null) {
				mijnResultset.first();
				resultaat = converteerHuidigeRijNaarObject(mijnResultset);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}

		return resultaat;
	}

	public static int voegFotoToe(Foto nieuweFoto) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("INSERT INTO Foto (Foto_id, Evenementen_id, Bestand) VALUES (?,?,?)", new Object[] { nieuweFoto.getFoto_id(), nieuweFoto.getEvenementen_id(), nieuweFoto.getBestand() });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	public static int updateFoto(Foto nieuweFoto) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("UPDATE Foto SET Bestand = ?  WHERE Foto_id = ?", new Object[] { nieuweFoto.getBestand() });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	public static int verwijderFoto(int Foto_id) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("DELETE FROM Foto WHERE Foto_id = ?", new Object[] { Foto_id });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	private static Foto converteerHuidigeRijNaarObject(ResultSet mijnResultset) throws SQLException {
		return new Foto(mijnResultset.getInt("Foto_id"), mijnResultset.getInt("Evenementen_id"), mijnResultset.getBytes("Bestand"));
	}

}
