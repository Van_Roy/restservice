package be.denes.partyradaradmin.dao;

import java.sql.*;
import java.util.ArrayList;

import be.denes.partyradaradmin.model.Promotie;
import java.time.Instant;
/**
 *
 * @author Maarten Heylen
 */
public class PromotieDao {

	public static ArrayList<Promotie> getPromoties() {
		ArrayList<Promotie> resultaat = new ArrayList<Promotie>();
		try {
			ResultSet mijnResultset = Database.voerSqlUitEnHaalResultaatOp("SELECT * from Promotie");
			if (mijnResultset != null) {
				while (mijnResultset.next()) {
					Promotie huidigePromotie = converteerHuidigeRijNaarObject(mijnResultset);
					resultaat.add(huidigePromotie);
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}

		return resultaat;
	}

	public static Promotie getPromotieById(int Promotie_id) {
		Promotie resultaat = null;
		try {
			ResultSet mijnResultset = Database.voerSqlUitEnHaalResultaatOp("SELECT * from Promotie where Promotie_id = ?", new Object[] { Promotie_id });
			if (mijnResultset != null) {
				mijnResultset.first();
				resultaat = converteerHuidigeRijNaarObject(mijnResultset);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}

		return resultaat;
	}

	public static int voegPromotieToe(Promotie nieuwePromotie) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("INSERT INTO Promotie (Promotie_id, Omschrijving) VALUES (?,?)", new Object[] { nieuwePromotie.getPromotie_id(), nieuwePromotie.getOmschrijving() });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	public static int updatePromotie(Promotie nieuwePromotie) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("UPDATE Promotie SET Omschrijving = ?  WHERE Promotie_id = ?", new Object[] { nieuwePromotie.getOmschrijving() });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	public static int verwijderPromotie(int Promotie_id) {
		int aantalAangepasteRijen = 0;
		try {
			aantalAangepasteRijen = Database.voerSqlUitEnHaalAantalAangepasteRijenOp("DELETE FROM Promotie WHERE Promotie_id = ?", new Object[] { Promotie_id });
		} catch (SQLException ex) {
			ex.printStackTrace();
			// Foutafhandeling naar keuze
		}
		return aantalAangepasteRijen;
	}

	private static Promotie converteerHuidigeRijNaarObject(ResultSet mijnResultset) throws SQLException {
            
		return new Promotie(mijnResultset.getInt("Promotie_id"), mijnResultset.getString("Omschrijving"));
	}

}
