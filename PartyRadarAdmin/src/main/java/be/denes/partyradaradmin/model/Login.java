/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.denes.partyradaradmin.model;

import java.sql.Blob;

/**
 *
 * @author Denes
 */
public class Login {
    private int Login_id;
    private String Naam;
    private String Info;
    private byte[] Profielfoto;
    private String Wachtwoord;

    public int getLogin_id() {
        return Login_id;
    }

    public void setLogin_id(int Login_id) {
        this.Login_id = Login_id;
    }

    public String getNaam() {
        return Naam;
    }

    public void setNaam(String Naam) {
        this.Naam = Naam;
    }

    public String getInfo() {
        return Info;
    }

    public void setInfo(String Info) {
        this.Info = Info;
    }

    public byte[] getProfielfoto() {
        return Profielfoto;
    }

    public void setProfielfoto(byte[] Profielfoto) {
        this.Profielfoto = Profielfoto;
    }

    public String getWachtwoord() {
        return Wachtwoord;
    }

    public void setWachtwoord(String Wachtwoord) {
        this.Wachtwoord = Wachtwoord;
    }

    public Login(int Login_id, String Naam, String Info, byte[] Profielfoto, String Wachtwoord) {
        this.Login_id = Login_id;
        this.Naam = Naam;
        this.Info = Info;
        this.Profielfoto = Profielfoto;
        this.Wachtwoord = Wachtwoord;
    }

    @Override
    public String toString() {
        return  this.Naam + " " + this.Info + " " + this.Wachtwoord;
    }
    
    
}
