/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.denes.partyradaradmin.model;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;


/**
 *
 * @author Denes
 */
public class Evenement {
    private int Evenement_id;
    private int Promotie_id;
    private String Titel;
    private String Beschrijving;
    private String Soort;
    private String Locatie;
    private Double Prijs;
    private ZonedDateTime Datum;
    private String Tijd;
    private String Categorie;

    public int getEvenement_id() {
        return Evenement_id;
    }

    public void setEvenementen_id(int Evenementen_id) {
        this.Evenement_id = Evenementen_id;
    }

    public int getPromotie_id() {
        return Promotie_id;
    }

    public void setPromotie_id(int Promotie_id) {
        this.Promotie_id = Promotie_id;
    }
    
    public String getTitel() {
        return Titel;
    }

    public void setTitel(String Titel) {
        this.Titel = Titel;
    }

    public String getBeschrijving() {
        return Beschrijving;
    }

    public void setBeschrijving(String Beschrijving) {
        this.Beschrijving = Beschrijving;
    }

    public String getSoort() {
        return Soort;
    }

    public void setSoort(String Soort) {
        this.Soort = Soort;
    }

    public String getLocatie() {
        return Locatie;
    }

    public void setLocatie(String Locatie) {
        this.Locatie = Locatie;
    }

    public Double getPrijs() {
        return Prijs;
    }

    public void setPrijs(Double Prijs) {
        this.Prijs = Prijs;
    }

    public ZonedDateTime getDatum() {
        return Datum;
    }

    public void setDatum(ZonedDateTime Datum) {
        this.Datum = Datum;
    }

    public String getTijd() {
        return Tijd;
    }

    public void setTijd(String Tijd) {
        this.Tijd = Tijd;
    }

    public String getCategorie() {
        return Categorie;
    }

    public void setCategorie(String Categorie) {
        this.Categorie = Categorie;
    }

    public Evenement(int Evenement_id, int Promotie_id, String Titel, String Beschrijving, String Soort, String Locatie, Double Prijs, ZonedDateTime Datum, String Tijd, String Categorie) {
        this.Evenement_id = Evenement_id;
        this.Promotie_id = Promotie_id;
        this.Titel = Titel;
        this.Beschrijving = Beschrijving;
        this.Soort = Soort;
        this.Locatie = Locatie;
        this.Prijs = Prijs;
        this.Datum = Datum;
        this.Tijd = Tijd;
        this.Categorie = Categorie;
    }
    
    public Evenement(){
        
    }
}
