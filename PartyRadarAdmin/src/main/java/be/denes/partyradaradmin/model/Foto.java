/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.denes.partyradaradmin.model;

import java.sql.Blob;

/**
 *
 * @author Denes
 */
public class Foto {
    private int Foto_id;
    private int Evenementen_id;
    private byte[] Bestand;

    public int getFoto_id() {
        return Foto_id;
    }

    public void setFoto_id(int Foto_id) {
        this.Foto_id = Foto_id;
    }

    public int getEvenementen_id() {
        return Evenementen_id;
    }

    public void setEvenementen_id(int Evenementen_id) {
        this.Evenementen_id = Evenementen_id;
    }

    public byte[] getBestand() {
        return Bestand;
    }

    public void setBestand(byte[] Bestand) {
        this.Bestand = Bestand;
    }

    public Foto(int Foto_id, int Evenementen_id, byte[] Bestand) {
        this.Foto_id = Foto_id;
        this.Evenementen_id = Evenementen_id;
        this.Bestand = Bestand;
    }
    
    
}
