/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.denes.partyradaradmin.model;

/**
 *
 * @author Denes
 */
public class Promotie {
    private int Promotie_id;
    private String Omschrijving;

    public int getPromotie_id() {
        return Promotie_id;
    }

    public void setPromotie_id(int Promotie_id) {
        this.Promotie_id = Promotie_id;
    }

    public String getOmschrijving() {
        return Omschrijving;
    }

    public void setOmschrijving(String Omschrijving) {
        this.Omschrijving = Omschrijving;
    }

    public Promotie(int Promotie_id, String Omschrijving) {
        this.Promotie_id = Promotie_id;
        this.Omschrijving = Omschrijving;
    }

    @Override
    public String toString() {
        return this.Omschrijving ; 
    }
    
    
}

