package be.denes.partyradaradmin.webservice;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import be.denes.partyradaradmin.dao.LoginDao;
import be.denes.partyradaradmin.model.Login;


/**
 *
 * @author Maarten Heylen
 */

@RestController
@RequestMapping("/Login")
public class LoginController {
	
	//OPMERKING: meestal wordt er niet zomaar doorverbonden naar methodes van de DAO
	//Meestal wordt hier nog extra code rondgeschreven.
	//Er moet ook aan security gedacht worden, een gebruiker zomaar toelaten alle gegevens uit de database te wissen is geen goed idee
	
	@RequestMapping("/getAll")
	public ArrayList<Login> getAll() {
		// Aanroepen met
		// http://localhost:8080/Login/getAll
		return LoginDao.getLogins();
	}
	


	@RequestMapping("/getById")
	public String getById(@RequestParam(value = "Login_id", defaultValue = "1") int Login_id) {
		// Aanroepen met
		// http://localhost:8080/Login/getById?Login_id=1

		return " " + LoginDao.getLoginById(Login_id);
	}
	
	@RequestMapping(value = "/voegToe", method = RequestMethod.POST)
	public int voegToe(@RequestBody Login nieuweLogin) {
		// Aanroepen met
		// http://localhost:8080/Logins/voegToe
		// Geef parameter mee in de body: {"naam":"Smartphone","prijs":299.99}
		// Content type van de POST request is application/json
		// Default constructor nodig bij Promotie-klasse voor automatische omzetting van JSON naar objecten
		
		
		return LoginDao.voegLoginToe(nieuweLogin);
	}
        
            @RequestMapping(value = "/wijzig", method = RequestMethod.POST)
            public int wijzig(@RequestBody Login nieuweLogin) {
            // Aanroepen met
            // http://localhost:8080/Login/wijzig
            // Geef parameter mee in de body: {"naam":"Mijn afspraak","beschrijving":"Dit is de beschrijving van mijn eerste afspraak"}
            // Content type van de POST request is application/json
            // Default constructor nodig bij Afspraak-klasse voor automatische omzetting van JSON naar objecten

            return LoginDao.updateLogin(nieuweLogin);
    }

            @RequestMapping(value = "/verwijder", method = RequestMethod.POST)
            public int verwijder(@RequestBody MultiValueMap<String, String> parameters) {
            // Aanroepen met
            // http://localhost:8080/Login/verwijder
            // Geef parameter mee in de body: Login_id=2
            // Content type van de POST request is application/x-www-form-urlencoded
        
            try {
                String loginIdAlsString = parameters.getFirst("Login_id");
                int Login_id = Integer.parseInt(loginIdAlsString);
                return LoginDao.verwijderLogin(Login_id);
            } catch (Exception ex) {
                return 0;
        }

    }
}
