package be.denes.partyradaradmin.webservice;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import be.denes.partyradaradmin.dao.EvenementDao;
import be.denes.partyradaradmin.dao.PromotieDao;
import be.denes.partyradaradmin.model.Evenement;
import be.denes.partyradaradmin.model.Promotie;


/**
 *
 * @author Maarten Heylen
 */

@RestController
@RequestMapping("/Evenement")
public class EvenementController {
	
	//OPMERKING: meestal wordt er niet zomaar doorverbonden naar methodes van de DAO
	//Meestal wordt hier nog extra code rondgeschreven.
	//Er moet ook aan security gedacht worden, een gebruiker zomaar toelaten alle gegevens uit de database te wissen is geen goed idee
	
	@RequestMapping("/getAll")
	public ArrayList<Evenement> getAll() {
		// Aanroepen met
		// http://localhost:8080/Evenement/getAll
		 return EvenementDao.getEvenementen(); 
	}
	

	@RequestMapping("/getById")
	public Evenement getById(@RequestParam(value = "Evenement_id", defaultValue = "1") int Evenement_id) {
		// Aanroepen met
		// http://localhost:8080/Evenement/getById?Evenement_id=1
		return EvenementDao.getEvenementById(Evenement_id);
	}
        
        
        
        @RequestMapping(value = "/voegToe", method = RequestMethod.POST)
	public int voegToe(@RequestBody Evenement nieuweEvenement) {
		// Aanroepen met
		// http://localhost:8080/Evenement/voegToe
		// Geef parameter mee in de body: {"naam":"Smartphone","prijs":299.99}
		// Content type van de POST request is application/json
		// Default constructor nodig bij Promotie-klasse voor automatische omzetting van JSON naar objecten
		
		
		return EvenementDao.voegEvenementToe(nieuweEvenement);
	}

         @RequestMapping(value = "/wijzig", method = RequestMethod.POST)
        public int wijzig(@RequestBody Evenement nieuweEvenement) {
        // Aanroepen met
        // http://localhost:8080/Evenement/wijzig
        // Geef parameter mee in de body: {"naam":"Mijn afspraak","beschrijving":"Dit is de beschrijving van mijn eerste afspraak"}
        // Content type van de POST request is application/json
        // Default constructor nodig bij Afspraak-klasse voor automatische omzetting van JSON naar objecten

        return EvenementDao.updateEvenement(nieuweEvenement);
    }

        @RequestMapping(value = "/verwijder", method = RequestMethod.POST)
        public int verwijder(@RequestBody MultiValueMap<String, String> parameters) {
        // Aanroepen met
        // http://localhost:8080/Evenement/verwijder
        // Geef parameter mee in de body: afspraakId=2
        // Content type van de POST request is application/x-www-form-urlencoded
        
        try {
            String evenementIdAlsString = parameters.getFirst("Evenement_id");
            int Evenement_id = Integer.parseInt(evenementIdAlsString);
            return EvenementDao.verwijderEvenement(Evenement_id);
        } catch (Exception ex) {
            return 0;
        }

    }
}
