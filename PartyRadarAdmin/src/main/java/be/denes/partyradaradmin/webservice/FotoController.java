package be.denes.partyradaradmin.webservice;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import be.denes.partyradaradmin.dao.FotoDao;
import be.denes.partyradaradmin.model.Foto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ResponseBody;




/**
 *
 * @author Maarten Heylen
 */

@RestController
@RequestMapping("/Foto")
public class FotoController {
	
	//OPMERKING: meestal wordt er niet zomaar doorverbonden naar methodes van de DAO
	//Meestal wordt hier nog extra code rondgeschreven.
	//Er moet ook aan security gedacht worden, een gebruiker zomaar toelaten alle gegevens uit de database te wissen is geen goed idee
	
	@RequestMapping("/getAll")
	public ArrayList<Foto> getAll() {
		// Aanroepen met
		// http://localhost:8080/Foto/getAll
		return FotoDao.getFotos();
	}
	


//	@RequestMapping("/getById")
//	public String getById(@RequestParam(value = "Foto_id", defaultValue = "1") int Foto_id) {
//		// Aanroepen met
//		// http://localhost:8080/Foto/getById?Foto_id=1
//
//		return " " + FotoDao.getFotoById(Foto_id);
//	}
        
        @ResponseBody
        @RequestMapping(value = "/getById", produces = MediaType.IMAGE_JPEG_VALUE)
        public byte[] getById(@RequestParam(value = "Foto_id", defaultValue = "1") int Foto_id){
            return FotoDao.getFotoById(Foto_id).getBestand();
        }
        
        
	
	@RequestMapping(value = "/voegToe", method = RequestMethod.POST)
	public int voegToe(@RequestBody Foto nieuweFoto) {
		// Aanroepen met
		// http://localhost:8080/Fotos/voegToe
		// Geef parameter mee in de body: {"naam":"Smartphone","prijs":299.99}
		// Content type van de POST request is application/json
		// Default constructor nodig bij Promotie-klasse voor automatische omzetting van JSON naar objecten
		
		
		return FotoDao.voegFotoToe(nieuweFoto);
	}
        
        @RequestMapping(value = "/wijzig", method = RequestMethod.POST)
        public int wijzig(@RequestBody Foto nieuweFoto) {
        // Aanroepen met
        // http://localhost:8080/Foto/wijzig
        // Geef parameter mee in de body: {"naam":"Mijn afspraak","beschrijving":"Dit is de beschrijving van mijn eerste afspraak"}
        // Content type van de POST request is application/json
        // Default constructor nodig bij Afspraak-klasse voor automatische omzetting van JSON naar objecten

        return FotoDao.updateFoto(nieuweFoto);
    }

        @RequestMapping(value = "/verwijder", method = RequestMethod.POST)
        public int verwijder(@RequestBody MultiValueMap<String, String> parameters) {
        // Aanroepen met
        // http://localhost:8080/Foto/verwijder
        // Geef parameter mee in de body: Foto_id=2
        // Content type van de POST request is application/x-www-form-urlencoded
        
        try {
            String fotoIdAlsString = parameters.getFirst("Foto_id");
            int Foto_id = Integer.parseInt(fotoIdAlsString);
            return FotoDao.verwijderFoto(Foto_id);
        } catch (Exception ex) {
            return 0;
        }

    }
}
