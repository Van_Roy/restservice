package be.denes.partyradaradmin.webservice;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import be.denes.partyradaradmin.dao.PromotieDao;
import be.denes.partyradaradmin.model.Promotie;


/**
 *
 * @author Maarten Heylen
 */

@RestController
@RequestMapping("/Promotie")
public class PromotieController {
	
	//OPMERKING: meestal wordt er niet zomaar doorverbonden naar methodes van de DAO
	//Meestal wordt hier nog extra code rondgeschreven.
	//Er moet ook aan security gedacht worden, een gebruiker zomaar toelaten alle gegevens uit de database te wissen is geen goed idee
	
	@RequestMapping("/getAll")
	public ArrayList<Promotie> getAll() {
		// Aanroepen met
		// http://localhost:8080/Promotie/getAll
		return PromotieDao.getPromoties();
                
	}
	


	@RequestMapping("/getById")
	public String getById(@RequestParam(value = "Promotie_id", defaultValue = "1") int Promotie_id) {
		// Aanroepen met
		// http://localhost:8080/Promotie/getById?Promotie_id=1

		return " " + PromotieDao.getPromotieById(Promotie_id);
	}
	
	@RequestMapping(value = "/voegToe", method = RequestMethod.POST)
	public int voegToe(@RequestBody Promotie nieuwePromotie) {
		// Aanroepen met
		// http://localhost:8080/Promotie/voegToe
		// Geef parameter mee in de body: {"naam":"Smartphone","prijs":299.99}
		// Content type van de POST request is application/json
		// Default constructor nodig bij Promotie-klasse voor automatische omzetting van JSON naar objecten
		
		
		return PromotieDao.voegPromotieToe(nieuwePromotie);
	}
        
            @RequestMapping(value = "/wijzig", method = RequestMethod.POST)
            public int wijzig(@RequestBody Promotie nieuwePromotie) {
            // Aanroepen met
            // http://localhost:8080/Promotie/wijzig
            // Geef parameter mee in de body: {"naam":"Mijn afspraak","beschrijving":"Dit is de beschrijving van mijn eerste afspraak"}
            // Content type van de POST request is application/json
            // Default constructor nodig bij Afspraak-klasse voor automatische omzetting van JSON naar objecten

            return PromotieDao.updatePromotie(nieuwePromotie);
    }

            @RequestMapping(value = "/verwijder", method = RequestMethod.POST)
            public int verwijder(@RequestBody MultiValueMap<String, String> parameters) {
            // Aanroepen met
            // http://localhost:8080/Promotie/verwijder
            // Geef parameter mee in de body: Foto_id=2
            // Content type van de POST request is application/x-www-form-urlencoded
        
            try {
                String promotieIdAlsString = parameters.getFirst("Promotie_id");
                int Promotie_id = Integer.parseInt(promotieIdAlsString);
                return PromotieDao.verwijderPromotie(Promotie_id);
            } catch (Exception ex) {
                return 0;
        }

    }
}
